<?php

API::ROUTE("POST /consultation/subjective/autosave", function(){
	user_perm_require("EP_WRITE");
    $req = API::PARAMETERS([
        'token' => '',
        'client' => 'hashid',
		'subjective' => 'allowBlank',
		'goals_short_term' => 'allowBlank',
		'goals_long_term' => 'allowBlank'
    ]);

    $userId = user_auth_getId($req['token']);
    user_require_client($userId, $req['client']);
    $db = DB::connect();

	$q = $db->prepare("
		SELECT `id` FROM `subjective`
		WHERE `client` = ? AND `status` = 'autosave'
	");
	$q->execute([$req['client']]);
	if($rowId = $q->fetchColumn()){
		//update
		$q = $db->prepare("
			UPDATE `subjective`
			SET `subjective` = ?,
			`goals_short_term` = ?,
			`goals_long_term` = ?
			WHERE `id` = ?
		");
		$q->execute([
			$req['subjective'],
			$req['goals_short_term'],
			$req['goals_long_term'],
			$rowId
		]);
		return $q->rowCount()==1;
	}
	else{
		//insert
		$q = $db->prepare("
			INSERT INTO `subjective`
			(`client`,`subjective`,`goals_short_term`, `goals_long_term`,`status`)
			VALUES (?,?,?,?, 'autosave')
		");
		$q->execute([
			$req['client'],
			$req['subjective'],
			$req['goals_short_term'],
			$req['goals_long_term']
		]);
		return $q->rowCount()==1;
	}
	API::FAIL("Failed to autosave");
});

API::ROUTE("POST /consultation/subjective/autosave/load", function(){
	user_perm_require("EP_READ");
    $req = API::PARAMETERS([
        'token' => '',
        'client' => 'hashid'
    ]);

    $userId = user_auth_getId($req['token']);
    user_require_client($userId, $req['client']);
    $db = DB::connect();

	$q = $db->prepare("
		SELECT `subjective`, `goals_short_term`, `goals_long_term`
		FROM `subjective`
		WHERE `client` = ? AND `status` = 'autosave'
	");
	$q->execute([$req['client']]);
	if($row = $q->fetch()){
		return $row;
	}
	return [
		'subjective' => '',
		'goals_short_term' => '',
		'goals_long_term' => ''
	];
});

API::ROUTE("POST /consultation/subjective/previous", function(){
	user_perm_require("EP_READ");
    $req = API::PARAMETERS([
        'token' => '',
        'client' => 'hashid'
    ]);

    $userId = user_auth_getId($req['token']);
    user_require_client($userId, $req['client']);
    $db = DB::connect();

	$q = $db->prepare("
		SELECT `subjective`, `goals_short_term`, `goals_long_term`, `timestamp`
		FROM `subjective`
		WHERE `client` = ? AND `status` = 'saved'
		ORDER BY `timestamp` DESC LIMIT 1
	");
	$q->execute([$req['client']]);
	if($row = $q->fetch()){
		$row['timestamp'] = strtotime($row['timestamp']);
		return $row;
	}
	API::FAIL("No record");
});

?>
