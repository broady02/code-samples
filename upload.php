<?php

API::ROUTE("POST /document/upload", function(){
	user_perm_require("CLIENT_WRITE");
	$req = API::PARAMETERS([
		'token' => '',
		'client' => 'hashid'
	]);
	$userId = user_auth_getId($req['token']);
	user_require_client($userId, $req['client']);

	if(!isset($_FILES['file'])){
		API::FAIL("No file uploaded");
	}
	$upload =  [
		'filename'=> $_FILES['file']['name'],
		'tempfile'=> $_FILES['file']['tmp_name'],
		'error'=>$_FILES['file']['error'],
		'ext'=>pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION),
	];
	if($upload['error'] !== 0){
		switch($upload['error']){
			case 1; API::FAIL('Failed to upload, file too large.'); break;
			case 2; API::FAIL('Failed to upload, file too large.'); break;
			case 3; API::FAIL('Failed to upload the whole file.'); break;
		}
		API::FAIL("Error uploading.", $upload['error']);
	}

	$db = DB::connect();

	$q = $db->prepare("
		INSERT INTO `uploads` (`client`, `status`, `filename`) VALUES (?, 'uploading', ?)
	");
	$q->execute([$req['client'], $upload['filename']]);
	$uploadId = $db->lastInsertId();

	$storage = API::GET('storage');
	$newFilePath = '_uploads/'.date('Y/m_d').'/';
	if(!is_dir($storage.$newFilePath)){
      mkdir($storage.$newFilePath, 0777, true);
    }
	$newFileName = HASHID::encode($uploadId, 'U').'.'.$upload['ext'];
	copy($upload['tempfile'], $storage.$newFilePath.$newFileName);
	$q = $db->prepare("
		UPDATE `uploads` SET
		`file` = ?, `status` = 'uploaded'
		WHERE `id` = ?
	");
	$q->execute([
		$newFilePath.$newFileName,
		$uploadId
	]);
	timezone_user($userId);
	$icon = 'file';
	if(in_array($upload['ext'], ['doc', 'docx'])){ $icon = 'word'; }
	if(in_array($upload['ext'], ['csv', 'xls', 'xlsx'])){ $icon = 'excel'; }
	if(in_array($upload['ext'], ['zip'])){ $icon = 'zip'; }
	if(in_array($upload['ext'], ['pdf'])){ $icon = 'pdf'; }
	client_add_record($req['client'], $upload['filename'], 'Uploaded '.date('M d, Y, g:i a'), 'upload', $uploadId, $icon);

	return true;
});

?>
