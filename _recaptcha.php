<?php

class RECAPTCHA{

	public static function verify($reCaptchaToken){
		$secret = API::GET('reCaptcha-secret-key');
		$ip = $_SERVER['REMOTE_ADDR'];
		$url = "https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$reCaptchaToken}&remoteip={$ip}";
		$options=array(
		    'ssl'=>array(
		        'cafile'            => 'cacert.pem',
		        'verify_peer'       => true,
		        'verify_peer_name'  => true,
		    ),
		);

		$context = stream_context_create( $options );
		return json_decode((file_get_contents( $url, FILE_TEXT, $context ) ))->success;
	}
};

?>
