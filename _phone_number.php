<?php

class PHONE_NUMBER{

	public $rawNumber = null;
    private $digits = null;
    private $valid = null;

	public function __construct($phoneNumber){
		$rawNumber = $phoneNumber;

        $phoneNumber = filter_var($rawNumber, FILTER_VALIDATE_REGEXP, ["options"=>[
            "regexp" => "/^(\(?0\d\)?(?: ?\d){8})$/"
        ]]);
        if($phoneNumber === false){
            $this->valid = false;
            return;
        }
        $this->valid = true;
        $digits = str_replace('(', '', $phoneNumber);
        $digits = str_replace(')', '', $digits);
        $digits = str_replace(' ', '', $digits);
        $this->digits = $digits;
	}

    public function isValid(){
        return $this->valid;
    }

    public function international(){
        return "+61".substr($this->digits, 1);
    }
}

?>
