var vm = new function(){
    var vm = this;

	vm.notes = ko.observable(ko.mapping.fromJS({
		'subjective': '',
		'goals_short_term': '',
		'goals_long_term': ''
	}));


	vm.loadAutosave = function(){
		return physkit.api("/consultation/subjective/autosave/load", {
			'token': Cookies.get('token'),
			'client': Cookies.get('clientId')
		})
		.then(function(notes){
			vm.notes(ko.mapping.fromJS(notes));
			$('.materialize-textarea').trigger('autoresize');
			vm.notes().subjective.subscribe(function(){ vm.autosave(); });
			vm.notes().goals_short_term.subscribe(function(){ vm.autosave(); });
			vm.notes().goals_long_term.subscribe(function(){ vm.autosave(); });
		});
	}

	vm.autosave = function(){
		data = ko.mapping.toJS(vm.notes());
		data['token'] = Cookies.get('token');
		data['client'] = Cookies.get('clientId');
		physkit.api_queue.queue("/consultation/subjective/autosave", data, 'subjective-autosave');
	}

	vm.recentSubjective = function(){
		vm.insertRecent('subjective');
	}
	vm.recentGoalsShortTerm = function(){
		vm.insertRecent('goals_short_term');
	}
	vm.recentGoalsLongTerm = function(){
		vm.insertRecent('goals_long_term');
	}

	vm.insertRecent = function(target){
		physkit.showLoader(true);
		physkit.api("/consultation/subjective/previous", {
			'token': Cookies.get('token'),
			'client': Cookies.get('clientId')
		})
		.then(function(previous){
			physkit.showLoader(false);
			if(target == 'subjective'){
				title = "Subjective Notes"
				textArea = vm.notes().subjective;
				previousText = previous.subjective;
			}
			if(target == 'goals_short_term'){
				title = "Short Term Goals"
				textArea = vm.notes().goals_short_term;
				previousText = previous.goals_short_term;
			}
			if(target == 'goals_long_term'){
				title = "Long Term Goals"
				textArea = vm.notes().goals_long_term;
				previousText = previous.goals_long_term;
			}
			physkit.confirm(
				title+" Submitted "+physkit.datetimeFormat(previous.timestamp, "LLL"),
				previousText,
				"Insert"
			)
			.then(function(){
				if(textArea() != ''){
					physkit.confirm("Are you sure about that?", "Inserting this entry will overwrite the current notes.")
					.then(function(){
						textArea(previousText);
						$('.materialize-textarea').trigger('autoresize');
						vm.autosave();

					});
				}
				else{
					textArea(previousText);
					$('.materialize-textarea').trigger('autoresize');
					vm.autosave();
				}
			});
		})
		.catch(function(e){
			physkit.showLoader(false);
			physkit.error("No previous record found.");
		});
	}

	vm.init = function(){
		physkit.showLoader(true);
		vm.loadAutosave()
		.then(function(){
			physkit.showLoader(false);
		});
	}

	vm.recentModal = new function(){
		var m = this;
		m.text = ko.observable('');
		m.open = function(){
			$('#recentModal').modal('open', {
				dismissible: false
			});
			$('#recentModal textarea').focus();
		};
		m.close = function(){
			$('#recentModal').modal('close');
		}
		m.insert = function(){

		}
	}


};
ko.applyBindings(vm, physkit.view);
setTimeout(vm.init, 10);
