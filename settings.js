var vm = new function(){
    var vm = this;
	vm.loaded = ko.observable(false);
	vm.phone = ko.observable('');
	vm.phoneAlt = ko.observable('');
	vm.businessLogoImg = ko.observable('');
	vm.signatureImg = ko.observable('');
    vm.unlockPin = ko.observable('');
    vm.timezone = ko.observable('');

    vm.signatureUpdate = function(){
      physkit.signature.sign()
      .then(function(signature){
        physkit.showLoader(true);
        physkit.api("/user/settings/signature/update", {
          'token': Cookies.get('token'),
          'signature': signature
        })
        .then(function(){
          physkit.alert('Signature updated.');
          physkit.showLoader(false);
		  physkit.navigate('settings');

        })
        .catch(function(e){
          physkit.error('Signature not updated.');
        });
      })
      .catch(function(e){
        physkit.error('Signature not updated.');
      });
    };

	vm.uploadLogoModal = new function(){
		var modal = this;
		modal.open = function(){
			$("#uploadLogoModal").modal('open', {
                dismissible: false
            });
		}
		modal.close = function(){
			$("#uploadLogoModal").modal('close');
		}
		modal.upload = function(){
			if($('#uploadLogoInput')[0].files[0] === undefined){
				physkit.error("Please select a document.");
				return;
			}
			if($('#uploadLogoInput')[0].files[0].size/1000000 > 10){
				physkit.error("Document must be less than 10MB.");
				return;
			}
			data = new FormData();
			data.append('token', Cookies.get('token'));
			data.append("file", $('#uploadLogoInput')[0].files[0]);

			var request = new XMLHttpRequest();
			request.open("POST", physkit.endpoint+'/user/settings/business-logo/update');
			var success = function(){
				physkit.showLoader(false);
				r = JSON.parse(this.response);
				if(r[0] === false){
					modal.close();
					physkit.alert("Logo Upload Failed", r[1]);
				}
				if(r[0] === true){
					modal.close();
					physkit.alert("Logo upload successful.")
					.then(function(){
						physkit.navigate('settings');
					});
				}

			}
			var error = function(){
				physkit.showLoader(false);
				modal.close();
				physkit.alert("Document Upload Failed");
			}
			request.addEventListener("load", success);
			request.addEventListener("error", error);
			physkit.showLoader(true);
			request.send(data);
		}
	}

	vm.phone.subscribe(function(val){
		if(vm.loaded()){
			physkit.api_queue.queue("/user/settings/phone/update", {
				token: Cookies.get('token'),
				phone: vm.phone()
			}, 'settings-phone');
		}

	});

	vm.phoneAlt.subscribe(function(val){
		if(vm.loaded()){
			physkit.api_queue.queue("/user/settings/phoneAlt/update", {
				token: Cookies.get('token'),
				phoneAlt: vm.phoneAlt()
			}, 'settings-phoneAlt');
		}
	});

    vm.timezoneSave = function(){
        physkit.showLoader(true);
        physkit.api("/user/settings/timezone/update", {
            token: Cookies.get('token'),
            timezone: vm.timezone()
        })
        .then(function(){
            physkit.alert('Timezone updated.');
            physkit.showLoader(false);
        })
        .catch(function(e){
            physkit.showLoader(false);
            physkit.error("Error updating.");
        });
    };

    vm.unlockPinSave = function(){
        physkit.showLoader(true);
        physkit.api("/user/settings/unlockPin/update", {
            'token': Cookies.get('token'),
            'unlockPin': vm.unlockPin()
        })
        .then(function(){
            Cookies.set("CMPIN", vm.unlockPin());
            physkit.alert('Client mode unlock PIN saved.');
            physkit.showLoader(false);
        })
        .catch(function(e){
            physkit.showLoader(false);
            physkit.error(e);
        });
    };

	vm.startup = function(){
		physkit.showLoader(true);
		physkit.api("/user/settings/load", {
			'token': Cookies.get('token')
		})
		.then(function(data){
			physkit.showLoader(false);
			vm.businessLogoImg("data:image/png;base64,"+data.businessLogo);
			vm.signatureImg("data:image/png;base64,"+data.signature);
			vm.phone(data.phone);
			vm.phoneAlt(data.phoneAlt);
            vm.unlockPin(data.unlockPin);
            vm.timezone(data.timezone);
			Materialize.updateTextFields();
			vm.loaded(true);
		});
	}
};

ko.applyBindings(vm, physkit.view);
setTimeout(vm.startup, 10);
